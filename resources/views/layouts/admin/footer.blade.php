 <!-- Footer Start -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
               Copyright &copy; {{date('Y')}} {{Setting::get('site_name' , 'CG-school')}}. {{tr('all_right_reserved')}}. 
            </div>
        </div>
    </div>
</footer>
<!-- end Footer -->
