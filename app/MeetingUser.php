<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingUser extends Model
{
 
 	public function meetingDetails() {

 		return $this->belongsTo(Meeting::class,'meeting_id');
 	} 

 	public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "MU-"."-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "MU-"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

    } 
}
